require 'sinatra'

configure do
  set :cache, Dalli::Client.new('localhost:11211')
end

get '/' do
  erb :index
end

post '/' do
  settings.cache.flush_all
  "已经清空了"
end
